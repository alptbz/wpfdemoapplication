﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace WpfRestPlayground.Models.FungeneratorsApi
{
    public class FunGenNameResponse
    {
        [JsonPropertyName("success")]
        public FunGenNameSuccess Success { get; set; }

        [JsonPropertyName("contents")]
        public FunGenNameContent Contents { get; set; }

        [JsonPropertyName("copyright")]
        public string Copyright { get; set; }

    }
}
