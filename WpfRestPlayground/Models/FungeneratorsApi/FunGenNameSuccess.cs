﻿using System.Text.Json.Serialization;

namespace WpfRestPlayground.Models.FungeneratorsApi
{
    public class FunGenNameSuccess
    {
        [JsonPropertyName("total")]
        public string Total { get; set; }

        [JsonPropertyName("start")]
        public int Start { get; set; }

        [JsonPropertyName("limit")]
        public int Limit { get; set; }
    }
}