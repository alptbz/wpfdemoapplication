﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace WpfRestPlayground.Models.SentimentApi
{
    public class SentimentRequest
    {
        public SentimentRequest()
        {
        }

        public SentimentRequest(string text)
        {
            Text = text;
        }

        [JsonPropertyName("text")]
        public string Text { get; set; }
    }
}
