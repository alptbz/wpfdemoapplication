﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace WpfRestPlayground.Models.AOECivilization
{
    public class CivilizationResponse
    {
        [JsonPropertyName("civilizations")]
        public List<Civilization> Civilizations { get; set; }

    }
}
