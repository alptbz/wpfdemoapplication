﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfRestPlayground
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public class TextBoxSentimentCtrlReturnImpl : ICommand
        {
            MainWindow main;

            public TextBoxSentimentCtrlReturnImpl(MainWindow main)
            {
                this.main = main;
            }

            public event EventHandler CanExecuteChanged;

            public bool CanExecute(object parameter)
            {
                return true;
            }

            public void Execute(object parameter)
            {
                main.buttonSentimentAnswer_Click(null, null);
            }
        }

        public ICommand textBoxSentimentCtrlReturn { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;
            textBoxSentimentCtrlReturn = new TextBoxSentimentCtrlReturnImpl(this);
        }

        private WebApiClient webApiClient = new WebApiClient();



        private async void buttonGetNewImage_Click(object sender, RoutedEventArgs e)
        {
            await Task.Run(() =>
            {
                var pictureFileName = webApiClient.GetMeAPicture();
                Dispatcher.Invoke(() =>
                {
                    image.Source = new BitmapImage(new Uri(pictureFileName));
                    labelRandomNameForPicture.Content = "";
                });
            });
        }

        private async void buttonGetNewFace_Click(object sender, RoutedEventArgs e)
        {
            
            await Task.Run(() =>
            {
                var pictureFileName = webApiClient.GetMeAFace();

                var randomNameTask = webApiClient.GetRandomName();
                randomNameTask.Wait();
                var randomName = randomNameTask.Result;
                Dispatcher.Invoke(() =>
                {
                    image.Source = new BitmapImage(new Uri(pictureFileName));
                    labelRandomNameForPicture.Content = randomName;
                });
            });
        }

        private async void buttonSentimentAnswer_Click(object sender, RoutedEventArgs e)
        {
            if (textBoxSentiment.Text != "")
            {
                textBoxSentimentResult.Text = await webApiClient.CheckSentiment(textBoxSentiment.Text);
            }
        }

        private void textBoxSentiment_TextChanged(object sender, TextChangedEventArgs e)
        {
            textBoxSentimentResult.Text = "";
        }

        private void textBoxSentiment_KeyDown(object sender, KeyEventArgs e)
        {
            
        }

        private void buttonClickMe_Click(object sender, RoutedEventArgs e)
        {
            // TODO: Display in TextBox entered name in MessageBox
            MessageBox.Show("Ente");
        }

        private async void buttonCivilzation_Click(object sender, RoutedEventArgs e)
        {
            await Task.Run(() =>
            {
                var randomNameTask = webApiClient.GetCivilization();
                randomNameTask.Wait();
                var randomName = randomNameTask.Result;
                Dispatcher.Invoke(() =>
                {
                    buttonCivilzation.Content = randomName;
                });
            });
        }
    }
}
