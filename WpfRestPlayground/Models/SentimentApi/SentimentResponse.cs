﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace WpfRestPlayground.Models.SentimentApi
{
    class SentimentResponse
    {
        [JsonPropertyName("result")]
        public Sentiment Result { get; set; }
        
        [JsonPropertyName("sentences")]
        public List<Sentence> Sentences { get; set; }
    }
}
