﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace WpfRestPlayground.Models.SentimentApi
{
    public class Sentiment
    {
        [JsonPropertyName("polarity")]
        public double Polarity { get; set; }

        [JsonPropertyName("type")]
        public string Type { get; set; }
    }
}
