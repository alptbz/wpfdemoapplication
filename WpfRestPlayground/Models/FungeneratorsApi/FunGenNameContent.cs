﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace WpfRestPlayground.Models.FungeneratorsApi
{
    public class FunGenNameContent
    {
        [JsonPropertyName("category")]
        public string Category { get; set; }

        [JsonPropertyName("variantion")]
        public string Variation { get; set; }

        [JsonPropertyName("names")]
        public List<string> Names { get; set; }
    }
}