﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using WpfRestPlayground.Models.AOECivilization;
using WpfRestPlayground.Models.FungeneratorsApi;
using WpfRestPlayground.Models.NameFake;
using WpfRestPlayground.Models.SentimentApi;

// https://docs.microsoft.com/en-us/dotnet/csharp/tutorials/console-webapiclient

namespace WpfRestPlayground
{
    /// <summary>
    /// Client side web api implementations
    /// </summary>
    public class WebApiClient
    {
        private static readonly WebClient client = new WebClient();
        private static readonly HttpClient httpClient = new HttpClient();

        private const string MEDIA_TYPE_APPLICATION_JSON = "application/json";

        public WebApiClient()
        {
            httpClient.DefaultRequestHeaders.Accept.Clear();
            httpClient.DefaultRequestHeaders.Accept.Add(
                new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue(MEDIA_TYPE_APPLICATION_JSON));
        }

        /// <summary>
        /// Downloads an image using the https://picsum.photos/ API and stores it in a temporary location. 
        /// </summary>
        /// <returns>path to a temporary file</returns>
        /// 
        public string GetMeAPicture()
        {
            var temporaryFileName = Path.GetTempFileName();
            client.DownloadFile(new Uri("https://picsum.photos/300/300"), temporaryFileName);
            return temporaryFileName;
        }

        /// <summary>
        /// Downloads an image from https://thispersondoesnotexist.com/ and stores it in a temporary location. 
        /// </summary>
        /// <returns>path to a temporary file</returns>
        public string GetMeAFace()
        {
            var temporaryFileName = Path.GetTempFileName();
            client.DownloadFile(new Uri("https://thispersondoesnotexist.com/image"), temporaryFileName);
            return temporaryFileName;
        }

        /// <summary>
        /// Send inputText to the Sentim API and returns a conclusion. 
        /// </summary>
        /// <param name="inputText">Input text in English</param>
        /// <returns>a conclusion in English</returns>
        public async Task<string> CheckSentiment(string inputText)
        {
            // https://stackoverflow.com/questions/10679214/how-do-you-set-the-content-type-header-for-an-httpclient-request
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "https://sentim-api.herokuapp.com/api/v1/");
            var requestContent = new SentimentRequest(inputText);
            // https://docs.microsoft.com/en-us/dotnet/standard/serialization/system-text-json-how-to?pivots=dotnet-5-0
            request.Content = new StringContent(JsonSerializer.Serialize(requestContent), Encoding.UTF8, MEDIA_TYPE_APPLICATION_JSON);

            var response = await httpClient.SendAsync(request);
            var contentString = await response.Content.ReadAsStringAsync();

            Console.WriteLine(contentString);

            var sentimentResponse = JsonSerializer.Deserialize<SentimentResponse>(contentString);

            Console.WriteLine(JsonSerializer.Serialize(sentimentResponse));

            if(sentimentResponse.Result.Type == "negative")
            {
                return $"I feel sorry for you :( \n(confidence: {sentimentResponse.Result.Polarity})";
            }
            if(sentimentResponse.Result.Type == "positive")
            {
                return $"I'm so happy for you :) \n(confidence: {sentimentResponse.Result.Polarity})";
            }
            return "Ok. Interesting!";
        }

        public async Task<string> GetRandomNameFunNameGenerator()
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "https://api.fungenerators.com/name/generate?category=french&limit=1");
            var response = await httpClient.SendAsync(request);
            var contentString = await response.Content.ReadAsStringAsync();

            Console.WriteLine(contentString);

            var funGenNameResponse = JsonSerializer.Deserialize<FunGenNameResponse>(contentString);

            Console.WriteLine(JsonSerializer.Serialize(funGenNameResponse));

            return funGenNameResponse.Contents.Names.First();
        }
        public async Task<string> GetRandomName()
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "https://api.namefake.com/");
            var response = await httpClient.SendAsync(request);
            var contentString = await response.Content.ReadAsStringAsync();

            Console.WriteLine(contentString);

            var funGenNameResponse = JsonSerializer.Deserialize<NameFakeResponse>(contentString);

            Console.WriteLine(JsonSerializer.Serialize(funGenNameResponse));

            return funGenNameResponse.Name;
        }

        public async Task<string> GetCivilization()
        {
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "https://age-of-empires-2-api.herokuapp.com/api/v1/civilizations");
            var response = await httpClient.SendAsync(request);
            var contentString = await response.Content.ReadAsStringAsync();

            Console.WriteLine(contentString);

            var funGenNameResponse = JsonSerializer.Deserialize<CivilizationResponse>(contentString);

            Console.WriteLine(JsonSerializer.Serialize(funGenNameResponse));

            Random r = new Random();

            return funGenNameResponse.Civilizations[r.Next(0, funGenNameResponse.Civilizations.Count)].Name;
        }

    }
}
