﻿using System.Text.Json.Serialization;

namespace WpfRestPlayground.Models.AOECivilization
{
    public class Civilization
    {

        [JsonPropertyName("name")]
        public string Name { get; set; }

    }
}