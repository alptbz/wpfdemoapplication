﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace WpfRestPlayground.Models.SentimentApi
{
    public class Sentence
    {
        [JsonPropertyName("sentence")]
        public string Sentence_ { get; set; }

        [JsonPropertyName("sentiment")]
        public Sentiment Sentiment { get; set; }


    }
}
